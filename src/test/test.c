#include "test.h"

#define _DEFAULT_SOURCE

#include "../mem.h"
#include "../mem_internals.h"
#include "memory.h"
#include "inttypes.h"
#include <sys/mman.h>


static struct block_header *get_block_header(const void *block) {
    return (struct block_header *) (((uint8_t *) block) - offsetof(struct block_header, contents));
}

static bool block_is_free(const void *block) {
    struct block_header *header = get_block_header(block);

    return header->is_free;
}

static void log_block_status(const char *prefix, const char *info, const void *memory) {
    const char *block_status = block_is_free(memory) ? "free" : "allocated";
    fprintf(stderr, "%s: %s - %s\n", prefix, info, block_status);
}



// regular memory allocation
bool test_1(void) {
    fputs("Starting test 1\n", stderr);

    void *allocated_memory = _malloc(200);
    bool is_allocated = !block_is_free(allocated_memory);

    log_block_status("TEST 1", "allocated block", allocated_memory);

    _free(allocated_memory);
    bool is_freed = block_is_free(allocated_memory);

    log_block_status("TEST 1", "allocated block", allocated_memory);

    return is_allocated && is_freed;
}

// free one out of several allocated blocks
bool test_2(void) {
    fputs("Starting test 2\n", stderr);

    void *block_1 = _malloc(100);
    bool block_1_init_allocation = !block_is_free(block_1);
    log_block_status("TEST 2", "block 1(init)", block_1);

    void *block_2 = _malloc(100);
    bool block_2_init_allocation = !block_is_free(block_2);
    log_block_status("TEST 2", "block 2(init)", block_2);


    _free(block_2);
    bool block_2_freed = block_is_free(block_2);
    log_block_status("TEST 2", "block 2(after freeing)", block_2);


    bool block_1_still_allocated = !block_is_free(block_1);
    log_block_status("TEST 2", "block 1(after freeing block 2)", block_1);

    _free(block_1);

    return block_1_init_allocation && block_2_init_allocation &&
           block_2_freed && block_1_still_allocated;
}

// free two out of several allocated blocks
bool test_3(void) {
    fputs("Starting test 3\n", stderr);

    void *block_1 = _malloc(100);
    bool block_1_init_allocation = !block_is_free(block_1);
    log_block_status("TEST 3", "block 1(init)", block_1);

    void *block_2 = _malloc(100);
    bool block_2_init_allocation = !block_is_free(block_2);
    log_block_status("TEST 3", "block 2(init)", block_2);

    void *block_3 = _malloc(100);
    bool block_3_init_allocation = !block_is_free(block_2);
    log_block_status("TEST 3", "block 3(init)", block_3);


    _free(block_3);
    bool block_3_freed = block_is_free(block_3);
    log_block_status("TEST 3", "block 3(after freeing)", block_3);

    _free(block_1);
    bool block_1_freed = block_is_free(block_1);
    log_block_status("TEST 3", "block 1(after freeing)", block_1);

    bool block_2_still_allocated = !block_is_free(block_2);
    log_block_status("TEST 3", "block 2(after freeing block 1 and 3)", block_2);

    _free(block_2);


    return block_1_init_allocation && block_2_init_allocation && block_3_init_allocation &&
           block_1_freed && block_3_freed && block_2_still_allocated;

}

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

// out of memory. New region extends previous
bool test_4(void) {
    fputs("Starting test 4\n", stderr);
    void *block_1 = _malloc(100);
    log_block_status("TEST 4", "tiny block 1", block_1);

    void *block_2 = _malloc(REGION_MIN_SIZE + 1); // more than init heap size
    log_block_status("TEST 4", "large block 2", block_2);

    struct block_header *block_1_header = get_block_header(block_1);
    struct block_header *block_2_header = get_block_header(block_2);

    void *addr_after_block_1 = block_after(block_1_header);

    size_t block_2_header_capacity = block_2_header->capacity.bytes;

    _free(block_2);
    _free(block_1);
    return block_2_header_capacity > REGION_MIN_SIZE &&         // heap was extended
           addr_after_block_1 == (void *) block_2_header; // new region extends previous
}

static void *map_region(void *addr) {
    return mmap((void *) addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}


// out of memory. New region does not extend previous
bool test_5(void) {
    fputs("Starting test 5\n", stderr);

    void *block_1 = _malloc(100);

    struct block_header *block_1_header = get_block_header(block_1);

    void *addr_after_block_1 = block_after(block_1_header);

    struct block_header *last_heap_header = block_1_header;

    while (last_heap_header->next) {
        last_heap_header = last_heap_header->next;
    }

    uint8_t *last_heap_addr = last_heap_header->capacity.bytes + last_heap_header->contents;

    map_region(last_heap_addr); // map dummy region

    void *block_2 = _malloc(REGION_MIN_SIZE * 4); // force map another region

    struct block_header *block_2_header = get_block_header(block_2);

    _free(block_1);
    _free(block_2);

    return addr_after_block_1 != (void *) block_2_header; // not continuous mapping
}
