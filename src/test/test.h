#ifndef LAB2_TEST_H
#define LAB2_TEST_H

#include "stdbool.h"

// regular memory allocation
bool test_1(void);

// free one out of several allocated blocks
bool test_2(void);

// free two out of several allocated blocks
bool test_3(void);

// out of memory. New region extends previous
bool test_4(void);

// out of memory. New region does not extend previous
bool test_5(void);

#endif //LAB2_TEST_H
