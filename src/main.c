#include <stdio.h>
#include "test/test.h"
#include "mem.h"
#include "mem_internals.h"

int main(void) {
    heap_init(REGION_MIN_SIZE);

    if( !test_1() ) {
        fputs("TEST 1 failed.\n", stderr);
        return -1;
    }

    if( !test_2() ){
        fputs("TEST 2 failed.\n", stderr);
        return -2;
    }

    if( !test_3() ){
        fputs("TEST 3 failed.\n", stderr);
        return -3;
    }

    if( !test_4() ){
        fputs("TEST 4 failed.\n", stderr);
        return -4;
    }

    if( !test_5() ){
        fputs("TEST 5 failed.\n", stderr);
        return -5;
    }

    fputs("All test completed successfully.\n", stderr);
    return 0;
}
